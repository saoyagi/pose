var IMAGE_WIDTH = 320
var IMAGE_HEIGHT = 240;





$(document).ready(function(){

    var indigator = $("#indigator");
    indigator.hide();

    $("#downloadImage").hide();

    var jCanvas = $("#canvas"),
        canvas = jCanvas.get(0),
        context = canvas.getContext("2d"),
        jVideo = $("#video"),
        video = jVideo.get(0),
        videoObj = { "video": true },
        errBack = function(error) {
            console.log("Video capture error: ", error.name);
        };

    jVideo.attr("width",IMAGE_WIDTH);
    jVideo.attr("height",IMAGE_HEIGHT);
    jCanvas.attr("width",IMAGE_WIDTH);
    jCanvas.attr("height",IMAGE_HEIGHT);

    //ビデオリスナーのセット
    if(navigator.getUserMedia) { // Standard
        navigator.getUserMedia(videoObj, function(stream) {
            video.src = stream;
            video.play();
        }, errBack);
    } else if(navigator.webkitGetUserMedia) { // WebKit-prefixed
        navigator.webkitGetUserMedia(videoObj, function(stream){
            video.src = window.webkitURL.createObjectURL(stream);
            video.play();
        }, errBack);
    } else if(navigator.mozGetUserMedia) { // Firefox-prefixed
        navigator.mozGetUserMedia(videoObj, function(stream){
            video.src = window.URL.createObjectURL(stream);
            video.play();
        }, errBack);
    }

    //撮影操作関数
    var snapPhoto = function(){
        //canvasに描画
        context.drawImage(video, 0, 0, IMAGE_WIDTH, IMAGE_HEIGHT);
        indigator.hide();
    }

    //遅延撮影
    $("#timer").on("click",function(){
        indigator.show();
        var count = 3;
        var counter = function(){
            $("#count").html(count)
            if(count == 0){
                clearTimeout(timer1);

                //snapPhoto();
                //convertImage();
                getPose(video);
            }
            count--;
        }
        timer1 = setInterval(counter,1000);



    });

    //すぐに撮影
    $("#snap").on("click", function() {
        snapPhoto();
        convertImage();

    });

    var convertImage = function(){

        //canvasの色を変えてみる
        var imageData = context.getImageData(0, 0, IMAGE_WIDTH, IMAGE_HEIGHT);
        var dataLength = imageData.height * imageData.width;
        var index = 0;
        for (var i = 0 ; i <dataLength ; i++ ){
            index = i * 4
            //色味を入れ替える
            imageData.data[index] = imageData.data[index+1]; //r
            imageData.data[index+1]  = imageData.data[index+2]//g
            imageData.data[index+2]  = imageData.data[index+3]//b
            imageData.data[index+3]  = imageData.data[index]//alpha
        }

        context.putImageData(imageData, 0, 0); //キャンバスに反映

        //pngに変換するテスト
        var image = new Image();

        var imageUrl = canvas.toDataURL("image/png");
        image.src = imageUrl;

        $("#imagePlace").empty();
        $("#imagePlace").append(image);

        $("#downloadImage").attr("href", imageUrl);
        $("#downloadImage").show();
    }

    //となりのインデックスを返す関数
    var getNeighborIndex = function(i,direction){
        switch(direction){
            case "up":
                //一番上の行じゃなければ
                if(i >= IMAGE_WIDTH){
                    return i - IMAGE_WIDTH;
                }else{
                    return null;
                }
                break;
            case "down":
                //一番下の行じゃなければ
                if(i < (IMAGE_WIDTH - 1)*IMAGE_HEIGHT){
                    return i - IMAGE_WIDTH;
                }else{
                    return null;
                }
                break;
            case "left":
                if((i+1) % IMAGE_WIDTH  == 1){
                    return null;
                }else{
                    return i - 1;
                }
                break;
            case "right":
                if((i+1) % IMAGE_WIDTH  == 0){
                    return null;
                }else{
                    return i + 1;
                }
                break;
        }

    }

    //sobelオペレータでエッジを検出
    var getEdge = function(){
        //todo 実装

        var imageData = context.getImageData(0, 0, IMAGE_WIDTH, IMAGE_HEIGHT);
        var dataLength = imageData.height * imageData.width;
        var index = 0;
        for (var i = 0 ; i <dataLength ; i++ ){
            index = i * 4
            //色味を入れ替える
            //imageData.data[index+0]  = imageData.data[index+0] * -1
            //imageData.data[index+1]  =
            //imageData.data[index+2]  =

        }

        context.putImageData(imageData, 0, 0); //キャンバスに反映

        //pngに変換するテスト
        var image = new Image();

        var imageUrl = canvas.toDataURL("image/png");
        image.src = imageUrl;

        $("#imagePlace").empty();
        $("#imagePlace").append(image);

        $("#downloadImage").attr("href", imageUrl);
        $("#downloadImage").show();
    }


    //ポーズを取得ボタン
    $("#getPose").on("click", function() {
        getPose(video);
        indigator.show();

    });




    //ポーズを取得
    var getPose = function(video){

        var numOfCanvas = $("#num").val() || 2;//差分をとる画像の枚数
        var interval = $("#sec").val() || 1; //差分をとる画像を難病置きに取得するか


        var canvases = {};
        var contexts = {};
        for(var i = 0; i < numOfCanvas; i++){
            //キャンバスエレメントの生成
            canvases[i] = document.createElement('canvas');
            canvases[i].setAttribute("width", IMAGE_WIDTH);
            canvases[i].setAttribute("height", IMAGE_HEIGHT);

            //コンテキスト取得
            contexts[i] = canvases[i].getContext("2d");


        }

        var count = 0;
        //タイマー起動
        var getPhotoTimerId = setInterval(function(){
                if(count >= numOfCanvas){
                    clearTimeout(getPhotoTimerId);
                    postCapture(contexts);
                    return;

                }
                //カメラから画像を取得してキャンバスとして保存
                contexts[count].drawImage(video, 0, 0, IMAGE_WIDTH, IMAGE_HEIGHT);
                count++;
            },
            interval);

        //rgb+alphaの画像の等質性を検証する関数
        var comparePixel = function(imageData, imageData2, num, mode){
            var result = null;

            //完全一致
            if(mode == "strict"){
                var boolR = imageData.data[num] == imageData2.data[num];
                var boolG = imageData.data[num + 1] == imageData2.data[num + 1];
                var boolB = imageData.data[num + 2] == imageData2.data[num + 2];
                //var boolA = imageData.data[num + 3] == imageData2.data[num + 3]
                result= (boolR && boolG) && boolB;
            }

            //共通の閾値をとる
            if(mode == undefined || mode == "default"){
                var threshold = 2;
                var boolR = imageData.data[num] > imageData2.data[num] - threshold && imageData.data[num] < imageData2.data[num] + threshold;
                var boolG = imageData.data[num + 1] > imageData2.data[num + 1] - threshold && imageData.data[num] < imageData2.data[num + 1] + threshold;
                var boolB = imageData.data[num + 2] > imageData2.data[num + 2] - threshold && imageData.data[num] < imageData2.data[num + 2] + threshold;
                result= (boolR && boolG) ||( (boolG && boolB) || (boolB && boolR));
            }

            return result;
        }





        //画像を取得し終わった後の処理
        var postCapture= function(contexts){
            var trueCount = {};

            $("#canvases").empty();


            var data = {}
            var dataLength = IMAGE_WIDTH * IMAGE_HEIGHT;
            var resultCanvas = document.createElement('canvas');
            resultCanvas.setAttribute("width", IMAGE_WIDTH);
            resultCanvas.setAttribute("height", IMAGE_HEIGHT);
            var resultContext = resultCanvas.getContext("2d");
            //var resultImageData = resultContext.createImageData(IMAGE_WIDTH, IMAGE_HEIGHT);
            //一番最初の画像をベースにする
            var resultImageData= contexts[0].getImageData(0, 0,IMAGE_WIDTH, IMAGE_HEIGHT);

            var compareRes = null;
            var index = 0;

            for(var i = 0; i < numOfCanvas; i++){
                data[i] = contexts[i].getImageData(0, 0,IMAGE_WIDTH, IMAGE_HEIGHT);
                $("#canvases").append(canvases[i]);

                trueCount[i] = {true:0, false:0}
                //すべての画像を比較
                for(var j = 0; j < dataLength; j++){
                    index = j * 4;
                    compareRes = comparePixel(resultImageData, data[i], index);

                    trueCount[i][compareRes]++; //数を確認しておく

                    if(!compareRes){

                        //前の絵と違うところは塗りつぶす
                        resultImageData.data[index] = 0;
                        resultImageData.data[index + 1] = 255; //r
                        resultImageData.data[index + 2] = 0;
                        resultImageData.data[index + 3] = 0;

                    }
                }


            }

            resultContext.putImageData(resultImageData, 0, 0);
            $("#result").empty();
            $("#result").append(resultCanvas);
            indigator.hide();

            console.log(JSON.stringify(trueCount));


        }

    }


});

