
var Pose = {
	IMAGE_WIDTH:320,
	IMAGE_HEIGHT:240,
	videoElement:null, //いろんなところで使うビデオ要素
	flgGetBackground:false,
    processing:false,
    getCanvas:function(){
        var resultCanvas = document.createElement('canvas');
        resultCanvas.setAttribute("width", Pose.IMAGE_WIDTH);
        resultCanvas.setAttribute("height", Pose.IMAGE_HEIGHT);
        return resultCanvas;
    }
}


$(document).ready(function(){
	initCamera();
	setEvents();
	background.init();
});


//インジゲータ操作用オブジェクト
var indigator = {
	on:function(){
		jIndigator = this.init();
        $("#instruction").hide();
        $("#count").hide();
		jIndigator.show().animate({
			height:"40px"
		});
	},
	off:function(){
		jIndigator = this.init();
		jIndigator.animate({
			height:"0"
		},function(){
			jIndigator.attr("height",0);
			jIndigator.hide();
            $("#instruction").show();
		});
		
	
	},
	init:function(){
		return $("#indigator");
	}

}

//背景撮影済みフラグ管理用オブジェクト
var background = {
	get:function(){
		$("#getPose").removeClass("disabled");
		 Pose.flgGetBackground = true;

	},
	init:function(){
		$("#getPose").addClass("disabled");
		 Pose.flgGetBackground = false;
	}
}

//webカメラの初期化
function initCamera(){
    Pose.videoElement = $("#video").get(0);
    var videoObj = { "video": true };
    var falseCallback = function(error) {
        console.log("Video capture error: ", error.name);
    };

    Pose.videoElement.setAttribute("width",Pose.MAGE_WIDTH);
    Pose.videoElement.setAttribute("height",Pose.IMAGE_HEIGHT);

	//ビデオリスナーのセット
    if(navigator.getUserMedia) { // Standard
        navigator.getUserMedia(videoObj, function(stream) {
            Pose.videoElement.src = stream;
            Pose.videoElement.play();
        }, falseCallback);
    } else if(navigator.webkitGetUserMedia) { // WebKit-prefixed
        navigator.webkitGetUserMedia(videoObj, function(stream){
            Pose.videoElement.src = window.webkitURL.createObjectURL(stream);
            Pose.videoElement.play();
        }, falseCallback);
    } else if(navigator.mozGetUserMedia) { // Firefox-prefixed
        navigator.mozGetUserMedia(videoObj, function(stream){
            Pose.videoElement.src = window.URL.createObjectURL(stream);
            Pose.videoElement.play();
        }, falseCallback);
    }

}

//ボタンクリックイベント系
function setEvents(){

	$("#onDebug").on("click",function(){
		$("#menuForDebug").show().animate({
			height:"24px"
		});
	});


    //撮影テスト
    $("#snap").on("click", function() {
        var canvas = snapPhoto();
        background.init();

        setImageUrl(canvas);

    
       
    });


    //背景撮影
    $("#getBackground").on("click",function(){

    	var bacngroundCanvas = snapPhoto();
    	Pose.backgroundContext = bacngroundCanvas.getContext("2d");

        Pose.backgroundImageData = Pose.backgroundContext.getImageData(0, 0, Pose.IMAGE_WIDTH, Pose.IMAGE_HEIGHT);

    	background.get();

        setImageUrl(bacngroundCanvas);

    	
    	
    });

    //ポーズ撮影
    $("#getPose").on("click",function(){
        if(Pose.processing == true){
            return false;
        }
        Pose.processing = true;
    	if (Pose.flgGetBackground == true){
	        var count = 3;
	        var counter = function(){
                $("#count").html(count);
                indigator.on();
	        	$("#count").show();
	           
	            if(count == 0){
	                clearTimeout(timer1);

	                var poseCanvas = snapPhoto();
	                Pose.poseContext = poseCanvas.getContext("2d");
	                Pose.poseImageData = Pose.poseContext.getImageData(0, 0, Pose.IMAGE_WIDTH, Pose.IMAGE_HEIGHT);


	     			indigator.off();
	     			
	                
					postCapProcess();
                    Pose.processing = false;

	            }
	            count--;
	        }
        timer1 = setInterval(counter,1000);

    	}else{
    		return false;
    	}
    })

    //学習用データ出力
    $("#lean").on("click",function(){
        startLearning();
    });



}

//撮影関数
function snapPhoto(){
    //canvasに描画
    var canvas = document.createElement('canvas');
    canvas.setAttribute("width", Pose.IMAGE_WIDTH);
    canvas.setAttribute("height", Pose.IMAGE_HEIGHT);
    context = canvas.getContext("2d"),
    context.drawImage(Pose.videoElement, 0, 0, Pose.IMAGE_WIDTH, Pose.IMAGE_HEIGHT);

    $("#results").empty();//キャンバスクリア
    $("#results").append(canvas);



    return canvas;
}


function setImageUrl(canvas){
    var image = new Image();
    var imageUrl = canvas.toDataURL("image/png");
 
    $("a#downloadImage").attr("href", imageUrl);
    $("a#downloadImage").show();
}

//ポーズの抽出

function postCapProcess(){
	//$("#results").empty(); //キャンバスクリア
    //var trueCount = {true:0, false:0}

    var resultCanvas = Pose.getCanvas();
    
    //差分画像の取得
    resultImageData = getDifference(resultCanvas, Pose.backgroundImageData, Pose.poseImageData);
    //$("#results").append(resultCanvas);

    //メディアンフィルタ
    //medianImageData = filter(resultImageData, resultCanvas, "median");
    //$("#results").append(resultCanvas);


    //ガウシャンフィルタ
    var c2 = Pose.getCanvas();
    data2 = filter(resultImageData, c2, "gausian", 64);
    //$("#results").append(resultCanvas2);

    var c3 = Pose.getCanvas();
    data3 = filter(resultImageData, c3, "gausian", 96);

    var c4 = Pose.getCanvas();
    data4 = filter(resultImageData, c4, "gausian", 128);

    //ガウシャン→メディアン
    var resultCanvas3 = Pose.getCanvas();
    filter(data2, resultCanvas3, "median", true);
    $("#results").append(resultCanvas3);

    var resultCanvas4 = Pose.getCanvas();
    filter(data3, resultCanvas4, "median", true);
    $("#results").append(resultCanvas4);

    var resultCanvas5 = Pose.getCanvas();
    filter(data4, resultCanvas5, "median", true);
    $("#results").append(resultCanvas5);

    //画像ダウンロード準備
    var image = new Image();
    var imageUrl = resultCanvas3.toDataURL("image/png");
     
    $("a#downloadImage").attr("href", imageUrl);
    $("a#downloadImage").show();

    //console.log(JSON.stringify(trueCount));//一致数
}


/*
方式2
*/
getDifference = function(resultCanvas, backgroundImageData, poseImageData, threshold){
    var resultImageData = resultCanvas.getContext("2d").createImageData(Pose.IMAGE_WIDTH, Pose.IMAGE_HEIGHT);
    var dataLength = Pose.IMAGE_WIDTH * Pose.IMAGE_HEIGHT;
    for(var j = 0; j < dataLength; j++){    
        index = j * 4;

        //差分
        var r = (backgroundImageData.data[index+0] - poseImageData.data[index+0] + 255)/2;
        var g = (backgroundImageData.data[index+1] - poseImageData.data[index+1] + 255)/2;
        var b = (backgroundImageData.data[index+2] - poseImageData.data[index+2] + 255)/2;
        
        //平均がしきい値以上なら
        var threshold2 =  threshold || 128
        var g2 =  0;
        if ((r + g + b) /3 > threshold2){
            g2 = 255;
        }
        //var g2 = (r + g + b) /3;
        resultImageData.data[index+0] = 0;
        resultImageData.data[index+1] = g2;
        resultImageData.data[index+2] = 0;
        resultImageData.data[index+3] = 255;

    }
    
    return resultImageData;
}

filter = function(imageData, canvas, name, binarize){
    var context = canvas.getContext("2d");
    var resultImageData = context.createImageData(Pose.IMAGE_WIDTH, Pose.IMAGE_HEIGHT);

    var dataLength = Pose.IMAGE_WIDTH * Pose.IMAGE_HEIGHT;
    for(var j = 0; j < dataLength; j++){    
        index = j * 4 + 1;

        var pixcelArray = []

        var top = 0;
        if(j > Pose.IMAGE_WIDTH){
            top = imageData.data[(j - Pose.IMAGE_WIDTH)*4 + 1];
        }
        pixcelArray.push(top);
        
        var topleft = 0;
        if(j > Pose.IMAGE_WIDTH +1){
            topleft = imageData.data[( j -Pose.IMAGE_WIDTH)*4];
        }
        pixcelArray.push(topleft);

        var topright = 0;
        if(j > Pose.IMAGE_WIDTH - 1){
            topright = imageData.data[(j- Pose.IMAGE_WIDTH)*4 + 2];
        }
        pixcelArray.push(topright);

        var bottom = 0;
        if(j < Pose.IMAGE_WIDTH * (Pose.IMAGE_HEIGHT - 1)){
            bottom = imageData.data[(j + Pose.IMAGE_WIDTH)*4 + 1];
        }     
        pixcelArray.push(bottom);  

        var bottomleft = 0;
        if(j < Pose.IMAGE_WIDTH * (Pose.IMAGE_HEIGHT - 1) +1 ){
            bottomleft = imageData.data[(j + Pose.IMAGE_WIDTH)*4 ];
        }
        pixcelArray.push(bottomleft);  

        var bottomright = 0;
        if(j < Pose.IMAGE_WIDTH * (Pose.IMAGE_HEIGHT - 1) -1){
            bottomright = imageData.data[(j + Pose.IMAGE_WIDTH)*4 + 2];
        }
        pixcelArray.push(bottomright);  

        var left = 0;
        if(j % Pose.IMAGE_WIDTH != 0){
            left = imageData.data[index - 4];
        }
        pixcelArray.push(left);  
            

        var right = 0;
        if(j % Pose.IMAGE_WIDTH != Pose.IMAGE_WIDTH -1) {
            right = imageData.data[index + 4];
        }
        pixcelArray.push(right); 


        //ディフォルト
        var rep = imageData.data[index];

        //メディアンフィルタ
        if(name == "median"){
            
            pixcelArray.sort(function(a, b){
                return a - b;    
            });
            rep = pixcelArray[Math.ceil(pixcelArray.length/2)]
                  
        }
   

        //ガウシャンフィルタ
        if(name == "gausian"){
            rep = (topleft + top*2  + topright + bottomleft + bottom*2  + bottomright + left*2 + right*2 + imageData.data[index]*3)/15;

        }
        
        //rとbはゼロ
        resultImageData.data[index - 1] = 0;
        resultImageData.data[index + 1] = 0;
        resultImageData.data[index + 2] = 255; //不透明

        //2値化
        if(binarize != false){
            var threshold = 128;
            if (typeof binarize == "number" ){
                threshold = binarize;
            }
            if(rep >=  threshold){
                rep = 255;    
            }else{
                rep= 0;
            }
        }

        resultImageData.data[index] = rep;
        
        

    }
     
    context.putImageData(resultImageData, 0, 0);
    return resultImageData;
}

//学習用データ出力
function startLearning(){
    var numOfCanvas = 1; //画像数
    //撮影間隔
    var interval = 100; 

    var canvases = [];
    var contexts = {};

    for(var i = 0; i < numOfCanvas; i++){
        //キャンバスエレメントの生成
        canvases.push(document.createElement('canvas'));
        canvases[i].setAttribute("width", Pose.IMAGE_WIDTH);
        canvases[i].setAttribute("height", Pose.IMAGE_HEIGHT);

        //コンテキスト取得
        contexts[i] = canvases[i].getContext("2d");


    }

    var count = 0;
    //タイマー起動
    var getPhotoTimerId = setInterval(function(){
            if(count >= numOfCanvas){
                clearTimeout(getPhotoTimerId);
                postCaptureForLearn(canvases);
                return;

            }
            //カメラから画像を取得してキャンバスとして保存
            contexts[count].drawImage(Pose.videoElement, 0, 0, Pose.IMAGE_WIDTH, Pose.IMAGE_HEIGHT);
            count++;
        },
        interval);


   
}


function postCaptureForLearn(canvases){
    $("#results").empty()

    for (var i = 0; i < canvases.length; i++){
        var canvas = canvases[i];
        var context = canvas.getContext("2d");
        var imageData = context.getImageData(0, 0, Pose.IMAGE_WIDTH, Pose.IMAGE_HEIGHT);

        var options = {
            defThreshold:128,
            filters:[
                {name: "gausian", threshold:96},
                {name: "median", threshold:96},
            ]
        };
        getOneTestResult(imageData, options);


    }
    //差分画像の取得
    


}

function getOneTestResult(options){
        
        var defImageData = getDifference(Pose.getCanvas(), Pose.backgroundImageData, imageData, options.defThreshold);

        context.putImageData(defImageData, 0, 0);
        
        //画像ダウンロード準備
        var image = new Image();
        var imageUrl = canvas.toDataURL("image/png");
        
        
        $("#results").append($("<br /><a href="+ imageUrl + "  download=" + i +" _def >" +   i + "_def</a>"))
        $("#results").append(canvas);

        //順番にフィルタする
        var canvas = null;
        var resultData = null;
        var targetData = defImageData;
        for (var i = 0; i < options.filters.length; i++){
            canvas = Pose.getCanvas();
            resultData = filter(targetData, c, options.filters[i].name, options.filters[i].threshold);
            targetData = resultData;
        }
        canvas
        
 

        var image = new Image();
        var imageUrl = c2.toDataURL("image/png");
        $("#results").append($("<a href="+ imageUrl + "  download=" + i +" _filtered >" +   i + "_filtered</a><br />"));
        $("#results").append(c2);

}
